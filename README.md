Potting mix nutrient calculator
===============================

[Hosted here](https://erikvv.gitlab.io/nutrient-ratio/)

Missing features:
- correct P2O5 and K2O
- save
- export
- share link
- gui to add custom ferts
- medium composition helper 
- choose between medium L or m2
- tooltips
