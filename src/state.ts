import { useState, Dispatch, SetStateAction } from 'react';
import { reject } from 'lodash-es';

export type State = {
    // hide some options to prevent overwhelming new users
    displayAdvanced: boolean,
    addedFerts: Array<AddedFert>
    // lock fertilizer ratio, 
    // allowing for an increase in dosage,
    // while keeping the same ratio
    ratioLocked: boolean,
    medium: {
        // liters
        baseAmount: number,
        totalAmount: number,
    }
}

/**
 * Input of the user
 */
export type AddedFert = {
    name: string,
    grams: number
}

const initialState: State = {
    addedFerts: [],
    ratioLocked: false,
    displayAdvanced: false,
    medium: {
        baseAmount: 1,
        totalAmount: 40,
    }
}

export const useAdditives = () => {
    const [state, setState]: [State, Dispatch<SetStateAction<State>>] = useState(initialState)

    const addAdditive = (name: string) => setState({
        ...state,
        addedFerts: [
            ...state.addedFerts,
            {
                name,
                grams: NaN
            }
        ]
    })

    const removeAdditive = (name: string) => setState({
        ...state,
        addedFerts: reject(state.addedFerts, {name})
    })

    const setAmount = (name: string, amount: number) => {
        const i = state.addedFerts.findIndex(a => a.name === name)
        const oldFert = state.addedFerts[i]
        const newFert = {
            ...oldFert,
            grams: amount
        }

        let ratio;
        if (state.ratioLocked) {
            ratio = oneIfInvalidRatio(newFert.grams / oldFert.grams)
        } else {
            ratio = 1;
        }

        let newFerts: AddedFert[] = state.addedFerts.map(fert => ({
            ...fert,
            grams: fert.grams * ratio,
        }))
        
        setState({
            ...state,
            addedFerts: [
                ...newFerts.slice(0, i),
                newFert,
                ...newFerts.slice(i+1, newFerts.length),
            ]
        })
    }

    const setBaseMediumAmount = (newBaseAmount: number) => {
        const oldBaseAmount = state.medium.baseAmount || 1
        const ratio = oneIfInvalidRatio(newBaseAmount / oldBaseAmount)

        const addedFerts = state.addedFerts.map(a => ({
            ...a,
            grams: a.grams * ratio
        }))

        setState({
            ...state,
            addedFerts,
            medium: {
                ...state.medium,
                baseAmount: newBaseAmount,
            }
        })
    }

    const setTotalMediumAmount = (totalAmount: number) => {
        setState({
            ...state,
            medium: {
                ...state.medium,
                totalAmount
            }
        })
    }

    const toggleRatioLocked = () => {
        setState({
            ...state,
            ratioLocked: !state.ratioLocked
        })
    }

    const toggleDisplayAdvanced = () => {
        setState({
            ...state,
            displayAdvanced: !state.displayAdvanced
        })
    }

    return {
        state, 
        addAdditive, 
        removeAdditive, 
        setAmount,
        setBaseMediumAmount,
        setTotalMediumAmount,
        toggleRatioLocked,
        toggleDisplayAdvanced,
    }
}

function oneIfInvalidRatio(value: number) {
    if (!value || value == Infinity) {
        return 1;
    } else {
        return value;
    }
}