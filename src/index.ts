import { render } from 'react-dom';
import { Fragment, createElement as h} from 'react';
import { additiveSelect } from './components/additive-select';
import { Table } from './components/table';
import { useAdditives } from './state';
import { GlobalStyle } from './style';
import { mediumComponent } from './components/medium.component';

const main = () => {
    const {
        state,
        state: {
            addedFerts,
            displayAdvanced,
            medium: {
                baseAmount: baseMediumAmount,
                totalAmount: totalMediumAmount
            }
        },
        addAdditive, 
        removeAdditive, 
        setAmount, 
        setBaseMediumAmount, 
        setTotalMediumAmount,
        toggleRatioLocked,
        toggleDisplayAdvanced,
    } = useAdditives()

    return h(Fragment, {}, 
        h(GlobalStyle),
        h(mediumComponent, {
            baseMediumAmount: state.medium.baseAmount, 
            totalMediumAmount: state.medium.totalAmount, 
            setBaseMediumAmount,
            setTotalMediumAmount,
        }),
        h('br'),
        h('br'),
        h(additiveSelect, {onSelect: addAdditive, addedFerts}),
        h('br'),
        h('br'),
        h(Table, {
            baseMediumAmount, 
            totalMediumAmount, 
            addedFerts, 
            setAmount, 
            ratioLocked: state.ratioLocked,
            toggleRatioLocked,
        }),
        h('br'),
        h('br'),
        h(AdvancedCheckbox, {displayAdvanced, toggleDisplayAdvanced}),
    );
}

const AdvancedCheckbox = ({displayAdvanced, toggleDisplayAdvanced}) => (
    h('label', {}, 
        h('input', 
            {type: 'checkbox', checked: displayAdvanced, onChange: toggleDisplayAdvanced},
        ),
        'More features'
    )
);

render(
    h(main),
    document.getElementById('react')
);
