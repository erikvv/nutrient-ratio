import { Global, css, jsx as h } from '@emotion/core'

export const GlobalStyle = () => h(Global, {
    styles: css`
        body {
            box-sizing: border-box;
            min-height: 100vh;
            padding: 1rem;
            margin: 0rem;
            color: white;

            display: flex;
            flex-direction: column;
            justify-content: space-between;

            background-color: #111;
            background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('black-soil.jpg')
        }
        footer {
            text-align: right;
        }
        a {
            /* light blue */
            color: rgb(150, 150, 255);
        }
    `
})