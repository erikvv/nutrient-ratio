

export const toMap = obj => new Map(Object.entries(obj));

/**
 * map() a Map to a new Map with the same keys
 */
export function mapMap<Key, OldValue, NewValue> (
    map: Map<Key, OldValue>, 
    fn: (v: OldValue, k: Key) => NewValue
) {
    const newMap = new Map();

    map.forEach((oldValue, key) => {
        newMap.set(key, fn(oldValue, key))
    })

    return newMap;
}

/**
 * Get all keys from an array of maps
 */
export function allKeys<K>(maps: Array<Map<K, any>>): Set<K> {
    const keys = new Set<K>();

    for (const map of maps) {
        for (const [key, _] of map) {
            keys.add(key);
        }
    }

    return keys;
}

export function orderSet<T>(set: Set<T>, order: Set<T>): Set<T> {
    set = new Set(set);
    const result = new Set<T>();

    for (const el of order) {
        if (set.has(el)) {
            set.delete(el);
            result.add(el);
        }
    }

    for (const remainder of set) {
        result.add(remainder);
    }

    return result;
}

export function orderMapKeysBySetAndFillBlanks<K, V>(
    map: Map<K,V>, 
    keyOrder: Set<K>,
    defaultValue: V
): Map<K, V> {
    const result = new Map<K,V>()

    for (const key of keyOrder) {
        result.set(key, map.get(key) ?? defaultValue)
    }

    return result
}