import { keyBy } from 'lodash-es';
import { toMap } from './util-fn';

export type Element = 'N' | 'P' | 'K' | 'S' | 'Ca' | 'Mg' | 'Fe' | string

type AdditiveInfo = {
    name: string
    elementMassRatios: Map<Element, number>
    info?: string
    url?: string
}

export const allAdditives: AdditiveInfo[] = [
    {
        name: "ECOstyle Vinassekali",
        info: "",
        elementMassRatios: toMap({
            N: .02,
            P2O5: .02,
            K2O: .30,
        })
    },
    {
        name: "FloraSelf Biorga 'Bed and Balcony'",
        elementMassRatios: toMap({
            N: .09,
            P2O5: .02,
            K2O: .04,
        }),
        info: "Seems all slow-release. Mf recommended dose 6-10 g/L depending on plant"
    },
    {
        name: "Franky’s Flora Guano",
        elementMassRatios: toMap({
            N: .13,
            P2O5: .12,
            K2O: .025,
        }),
        info: "Bird Guano NPK 13-12-2.5. Mfg recommended dose 1-5 g/L but some super soil recipes go up to 15 g/L"
    },
    {
        name: "Franky’s Flora lavameel",
        elementMassRatios: toMap({
            // content of compound x molecule mass ratio
            Ca: .125 * .715,
            Fe: .11 * .7,
            Mg: .05 * .6,
            K2O: .04,
        }),
        info: "Mf recommended dose 1-2.5 g/L but some recipes go up to 10 g/L and more"
    },
    {
        name: "Kieserite",
        elementMassRatios: toMap({
            Mg: .175,
            S: .22,
        }),
        info: "MgSO4·H2O",
    },
    {
        name: "DCM Green Lime",
        elementMassRatios: toMap({
            Mg: .09, // .15 * .6
            Ca: .3, // Just eyeballing CaCO3 => 40% Ca minus some for the Mg
        }),
        info: "magnesium calcium carbonate. Specified interval 50 - 15 % MgO"
    }
];

export const allAdditivesByName = keyBy(allAdditives, 'name');
