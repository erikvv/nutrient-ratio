import { AddedFert } from '../state';
import { jsx as h, css} from '@emotion/core';
import { allAdditivesByName, Element } from '../all-additives';
import { mapMap, allKeys, orderSet, orderMapKeysBySetAndFillBlanks } from '../util-fn';
import { map } from 'iter-tools';
import { GramInput } from './number-input';

const displayGrams = (grams?: number): string => {
    if (!grams) {
        return '';
    }

    if (grams >= 100) {
        return Math.round(grams).toString() + ' g';
    } else {
        return grams.toPrecision(2) + ' g';
    }
}

const displayPercentage = (ratio?: number): string => {
    if (!ratio) {
        return '';
    }

    return (ratio * 100).toFixed(1) + ' %';
}

const order = new Set<Element>(['N', 'P2O5', 'K2O', 'S', 'Ca', 'Mg', 'Fe'])

type TableData = {
    elementOrder: Set<Element>, // for table headers
    ferts: Array<ComputedAddedFert>
    totals: {
        relativeElementGrams: Map<Element, number>,
        absoluteElementGrams: Map<Element, number>,
    }
    combinedRatio: Map<Element, number>, // NPK of the added fertilizers as if it were its own product
}

type ComputedAddedFert = {
    name: string,
    grams: number,
    relativeElementGrams: Map<Element, number>, // elemental nutrient per 1 L of medium, or other chosen base
    absoluteElementGrams: Map<Element, number> // elemental nutrient per total medium
}

export const Table = ({
    baseMediumAmount, 
    totalMediumAmount, 
    addedFerts, 
    setAmount,
    ratioLocked,
    toggleRatioLocked,
}: {
    baseMediumAmount: number,
    totalMediumAmount: number,
    addedFerts: AddedFert[], 
    setAmount: (element: Element, amount: number) => void,
    ratioLocked: boolean,
    toggleRatioLocked: () => void,
}) => {
    const ratio = totalMediumAmount / baseMediumAmount
    const table = calcTable(addedFerts, ratio);

    const elementOrder = table.elementOrder;

    const baseDisplayAmount = baseMediumAmount == 1 ? '' : baseMediumAmount;

    return (
        h('table', {css: css`
                border-collapse: collapse;
                & td {
                    padding: .2ch 1ch;
                }
                & tbody tr:nth-child(odd) {
                    background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5));
                }
                td:nth-child(n + 3) {
                    text-align: right;
                }
            `},
            h('thead', {}, 
                h('tr', {}, 
                    h('th', {}, 'Name'),
                    h('th', {}, `g/${baseDisplayAmount}L`),
                    h('th', {}, 'Total'),
                    // ...Array.from(map(el => h('th', {}, el), elementOrder)),
                    ...Array.from(map(el => h('th', {}, el), elementOrder))
                )
            ),
            h('tbody', {}, 
                ...table.ferts.map(fert => 
                    h('tr', {}, 
                        h('td', {}, fert.name),
                        h('td', {}, 
                            h(GramInput, {value: fert.grams, onInput: n => setAmount(fert.name, n)})
                        ),
                        h('td', {}, displayGrams(fert.grams * ratio)),
                        // ...Array.from(map(
                        //     el => h('td', {}, 
                        //         displayGrams(fert.relativeElementGrams.get(el))
                        //     ),
                        //     elementOrder
                        // )),
                        ...Array.from(map(
                            el => h('td', {}, 
                                displayGrams(fert.absoluteElementGrams.get(el))
                            ), 
                            elementOrder
                        )),
                    )
                ),
                addedFerts.length > 1 && h('tr', {}, 
                    h('td', {}, 'TOTAL'),
                    h('td', {}, h(LockRatioCheckbox, {ratioLocked, toggleRatioLocked})),
                    h('td'),
                    // ...Array.from(map(
                    //     el => h('td', {}, 
                    //         displayGrams(table.totals.relativeElementGrams.get(el))
                    //     ), 
                    //     elementOrder
                    // )),
                    ...Array.from(map(
                        el => h('td', {}, 
                            displayGrams(table.totals.absoluteElementGrams.get(el))
                        ), 
                        elementOrder
                    )),
                ),
                addedFerts.length > 1 && h('tr', {}, 
                    h('td', {}, 'Combined NPK'),
                    h('td'),
                    h('td'),
                    ...Array.from(map(
                        el => h('td', {}, 
                            displayPercentage(table.combinedRatio.get(el))
                        ), 
                        elementOrder
                    )),
                )
            )
        )
    )
}

const LockRatioCheckbox = ({ratioLocked, toggleRatioLocked}) => (
    h('label', {}, 
        h('input', 
          {type: 'checkbox', checked: ratioLocked, onChange: toggleRatioLocked},
        ),
        'Lock ratio'
    )
);

function calcTable(
    addedFerts: AddedFert[], 
    ratio: number
): TableData {
    const elementAmounts = addedFerts.map(calculateElementAmount);
    const elements = allKeys(elementAmounts);
    const elementOrder = orderSet(elements, order);

    const ferts: Array<ComputedAddedFert> = addedFerts.map(addedFert => {
        const relativeElementGrams = orderMapKeysBySetAndFillBlanks(calculateElementAmount(addedFert), elementOrder, NaN);

        return {
            name: addedFert.name,
            grams: addedFert.grams,
            relativeElementGrams: relativeElementGrams,
            absoluteElementGrams: multiply(relativeElementGrams, ratio),
        }
    });

    return {
        elementOrder,
        ferts,
        totals: {
            relativeElementGrams: totalElementAmounts(ferts.map(f => f.relativeElementGrams)),
            absoluteElementGrams: totalElementAmounts(ferts.map(f => f.absoluteElementGrams)),
        },
        combinedRatio: calculateCombinedRatio(ferts)
    };
}

function totalElementAmounts(
    elementAmountsCollection: Array<Map<Element, number>>
): Map<Element, number> {
    const result = new Map<Element, number>();

    for (const elementAmounts of elementAmountsCollection) {
        for (const [element, elementAmount] of elementAmounts) {
            result.set(element, (result.get(element) || 0) + (elementAmount || 0));
        }
    }

    return result;
}

function multiply(
    elementAmounts: Map<Element, number>, 
    factor: number
): Map<Element, number> {
    return mapMap(elementAmounts, baseAmount => baseAmount * factor);
}

function calculateElementAmount(addedFert: AddedFert): Map<Element, number> {
    if (!addedFert.grams) {
        return new Map();
    }

    const additiveInfo = allAdditivesByName[addedFert.name]

    return mapMap(additiveInfo.elementMassRatios, ratio => ratio * addedFert.grams);
}

/**
 * Calculate NPK of the added fertilizers as if it were a single product
 */
function calculateCombinedRatio(addedFerts: ComputedAddedFert[]): Map<Element, number> {
    const relativeFertilizerGrams: number = relativeFertilizerAmount(addedFerts);

    const relativeElementGrams = totalElementAmounts(addedFerts.map(f => f.relativeElementGrams));

    return mapMap(
        relativeElementGrams,
        r => r / relativeFertilizerGrams
    );
}

/**
 * Add the total mass of all added fertilizers regardless of their composition
 */
function relativeFertilizerAmount(addedFerts: AddedFert[]): number {
    return addedFerts.reduce<number>(
        (totalGrams: number, a: AddedFert) => totalGrams + (a.grams || 0),
        0
    );
}
