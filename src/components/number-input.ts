
import { jsx as h, css} from '@emotion/core'
import { useState } from 'react'
import { eq } from 'lodash-es';

// export const numberInput = ({value, onInput, unit, width = '4ch'}: {
//     value?: number, 
//     onInput: (n: string) => void, 
//     unit: string
//     width?: string
// }) => (
//     h('div', {}, 
//         h('input', {
//             value: value || '',
//             onChange: event => onInput((event.target as HTMLInputElement).value),
//             type: 'number',
//             step: .001,
//             css: {
//                 width,
//             }
//         }),
//         ' ',
//         unit
//     )
// )

/**
 * An input which only accepts and emits values of type number.
 * Emits NaN if the string in the input is not a number.
 * 
 * I don't want this component to exist because of the large surface area for bugs.
 * 
 * We only need this to use typescripts strictNullChecks.
 * In plain JS it's usually fine and actually simpler to emit a string which is not a number 
 * because it'll just be coerced to 0 or NaN during calculations
 */
export const NumberInput = ({value, onInput, width = '4ch'}) => {
    if (typeof value !== "number") {
        throw new Error('numberInput only accepts values of type number, got ' + value)
    }

    const propStringValue = numberToString(value);

    const [stringValue, setStringValue] = useState(propStringValue);

    const currentValue = stringToNumber(stringValue);

    // check if the value is being changed externally
    if (!equals(value, currentValue)) {
        setStringValue(propStringValue)
    }

    return h('input', {
        value: stringValue,
        onChange: event => {
            const newStringValue = (event.target as HTMLInputElement).value
            setStringValue(newStringValue)
            onInput(stringToNumber(newStringValue))
        },
        type: 'number',
        step: .001,
        css: {
            width,
        }
    })
}

/**
 * Compare 2 numbers, taking care of NaN's
 * 
 * NaN + NaN => same
 * NaN + 1 => different
 * 1 + NaN => different
 * 1 + 1 => same 
 * 1 + 2 => different
 */
function equals(n1: number, n2: number) {
    if (Number.isNaN(n1) && Number.isNaN(n2)) {
        return true;
    }

    return n1 === n2;
}

function stringToNumber(stringValue: string): number {
    if (stringValue === '') {
        return NaN;
    } else {
        return Number(stringValue);
    }
}

function numberToString(numberValue: number): string {
    return Number.isFinite(numberValue) ? String(numberValue) : ''
}

export const NumberWithUnit = ({unit, ...props}) => (
    h('div', {}, 
        h(NumberInput, props),
        ' ',
        unit
    )
)

export const LiterInput = ({value, onInput}) => NumberWithUnit({value, onInput, unit: 'L'})

export const GramInput = ({value, onInput}) => NumberWithUnit({value, onInput, unit: 'g'})
