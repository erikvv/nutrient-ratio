
import { createElement as h } from 'react'
import { allAdditives } from '../all-additives'
import { reject } from 'lodash-es';
import { AddedFert } from '../state';

export const additiveSelect = (
    {onSelect, addedFerts}: {onSelect: (name: string) => void, addedFerts: AddedFert[]}
) => {
    const onChange = event => {
        const name = event.target.value;
        //const selected = allAdditives.find(add => add.name === name)

        if (name) {
            onSelect(name)
        }

        // revert to selecting the default
        event.target.value = '';
    }

    const namesOfAddedFerts = addedFerts.map(a => a.name);
    const options = reject(allAdditives, a => namesOfAddedFerts.includes(a.name));

    return h('select', 
        {onChange},
        h('option', {value: ''}, '--- add fertilizer ---'),
        ...options.map(
            additive => h('option', {value: additive.name}, additive.name)
        )
    )
}