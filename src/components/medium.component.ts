import { LiterInput } from './number-input'
import { jsx as h, css} from '@emotion/core';

const rightPadding = {
    paddingRight: '1ch',
}

export const mediumComponent = ({
    totalMediumAmount, 
    baseMediumAmount, 
    setTotalMediumAmount, 
    setBaseMediumAmount
}) => (
    h('div', {
            css: {
                display: 'inline-grid',
                gridTemplateColumns: 'auto auto',
            }
        }, 
        h('div', {css: rightPadding}, 'The total medium quantity is'),
        h(LiterInput, {
            value: totalMediumAmount,
            onInput: liters => setTotalMediumAmount(liters)
        }),
        h('div', {css: rightPadding}, 'You are adding ferts per'),
        h(LiterInput, {
            value: baseMediumAmount,
            onInput: liters => setBaseMediumAmount(liters)
        }),
    )
)